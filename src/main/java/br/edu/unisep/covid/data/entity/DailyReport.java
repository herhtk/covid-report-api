package br.edu.unisep.covid.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "daily_report")
public class DailyReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_report")
    private Integer id;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "recovered")
    private Integer recovered;

    @Column(name = "deaths")
    private Integer deaths;

    @Column(name = "infected")
    private Integer infected;

    @Column(name = "discarded")
    private Integer discarded;

}
